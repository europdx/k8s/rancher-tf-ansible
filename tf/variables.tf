variable "github_client_id" {
    default = ""
}

variable "github_client_secret" {
    default = ""
}

variable "rancher2_access_key" {
    default = ""
}

variable "rancher2_secret_key" {
    default = ""
}

variable "radim_github" {
    default = "github_user://15783625"
}

variable "lubo_github" {
    default = "github_user://41989919"
}
variable "rsasinka_github" {
    default = "github_user://60224262"
}

variable "dstuchlik_github" {
    default = "github_user://52158546"
}

variable "akrenek_github" {
    default = "github_user://8985711"
}

variable "dkouril_github" {
    default = "github_user://1067311"
}

variable "zdudova_github" {
    default = "github_user://23636100"
}

variable "jhandl_github" {
    default = "github_user://46920109"
}
variable "musman_github" {
    default = "github_user://20109255"
}


variable "github_acess_mode" {
    default = "required"
}

variable "cluster_name" {
    default = "europdx"
}

variable "rancher_url" {
    default = "https://rancher.edirex.ics.muni.cz"
}

variable "password" {
    default = ""
}
