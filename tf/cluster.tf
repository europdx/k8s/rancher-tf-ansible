resource "rancher2_cluster" "europdx" {
  name = var.cluster_name
  description = "Our beautiful cluster"
  rke_config {
    network {
      plugin = "canal"
    }
  }
  enable_cluster_monitoring = true
  enable_network_policy = true
}
