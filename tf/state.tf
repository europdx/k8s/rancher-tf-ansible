terraform {
  backend "swift" {
    container         = "rancher_edirex_terraform_state"
    archive_container = "rancher_edirex_terraform-state-archive"
  }
}
