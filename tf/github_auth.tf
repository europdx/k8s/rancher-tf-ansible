resource "rancher2_auth_config_github" "github" {
  client_id = var.github_client_id
  client_secret = var.github_client_secret
  access_mode = var.github_acess_mode
  allowed_principal_ids = ["${var.lubo_github}", "${var.radim_github}", "${var.rsasinka_github}", "${var.dstuchlik_github}", "${var.akrenek_github}", "${var.dkouril_github}", "${var.zdudova_github}", "${var.jhandl_github}", "${var.musman_github}"]
}
