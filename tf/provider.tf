provider "rancher2" {
  alias = "bootstrap"

  api_url = var.rancher_url
  bootstrap = true
}

resource "rancher2_bootstrap" "admin" {
  provider = rancher2.bootstrap
  current_password = var.password
  password = var.password
  telemetry = true
}


provider "rancher2" {
  api_url = rancher2_bootstrap.admin.url
  token_key = rancher2_bootstrap.admin.token
}


provider "openstack" {
  auth_url = "https://identity.cloud.muni.cz/v3"

  region = "brno1"
}
