#!/bin/bash
set -x
COMPOSEDIR=/home/pdxuser/dataportal-docker
LETSDIR=/etc/letsencrypt/live/dataportal.europdx.eu
RENEWEDFLAG=/tmp/renewed.txt

sudo certbot renew --renew-hook "touch $RENEWEDFLAG"
if [ -f $RENEWEDFLAG ]; then
   echo "Just renewed."
   cd $COMPOSEDIR
   /usr/local/bin/docker-compose down
   sudo cp $LETSDIR/privkey.pem $COMPOSEDIR/proxy/ssl/server.key
   sudo cp $LETSDIR/fullchain.pem $COMPOSEDIR/proxy/ssl/server.crt
   sudo chown pdxuser:pdxuser $COMPOSEDIR/proxy/ssl/server.key
   sudo chown pdxuser:pdxuser $COMPOSEDIR/proxy/ssl/server.crt
   rm $RENEWEDFLAG
   /usr/local/bin/docker-compose up -d
else
   echo "Not renewed."
fi