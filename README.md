# Rancher
========

Rancher v2.2.7
--------------

## Requirements (1 node setup)
------------------------------
    2vcpu 	8 GB
    Recommendation: always using SSD disks that allows the maximum IOPs for Ranchcer server as performance can decrease...

    More info: https://rancher.com/docs/rancher/v2.x/en/installation/requirements/

# Ansible

## Installation of Rancher Master
1. Set destination host in ansible_hosts `[barn]` section.  
2. Run `ansible-playbook setup-barn.yml` with 'tf-edirex' key

## A
3. Login to rancher
4. set up Authentication https://rancher.com/docs/rancher/v2.x/en/admin-settings/authentication/github/


## B
3. Run `terraform init` & `terraform plan` & `terraform apply` (This will create cluster and setup authentication in Rancher)
